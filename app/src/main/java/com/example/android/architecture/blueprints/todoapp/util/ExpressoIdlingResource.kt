package com.example.android.architecture.blueprints.todoapp.util

import androidx.test.espresso.idling.CountingIdlingResource

/**
 * Here's an example of how you'd use EspressoIdlingResource:
 * EspressoIdlingResource.increment()
 * try {
 *   doSomethingThatTakesALongTime()
 * } finally {
 *   EspressoIdlingResource.decrement()
 * }
 * */
object EspressoIdlingResource {

    private const val RESOURCE = "GLOBAL"

    @JvmField
    val countingIdlingResource = CountingIdlingResource(RESOURCE)

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {
        if (!countingIdlingResource.isIdleNow) {
            countingIdlingResource.decrement()
        }
    }
}

/**
 * Here's an example of how you'd use wrapEspressoIdlingResource:
 * wrapEspressoIdlingResource {
 *   doWorkThatTakesALongTime()
 * }
 * */
inline fun <T> wrapEspressoIdlingResource(function: () -> T): T {
    // Espresso does not work well with coroutines yet. See
    // https://github.com/Kotlin/kotlinx.coroutines/issues/982
    EspressoIdlingResource.increment() // Set app as busy.
    return try {
        function()
    } finally {
        EspressoIdlingResource.decrement() // Set app as idle.
    }
}