package com.example.android.architecture.blueprints.todoapp

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.example.android.architecture.blueprints.todoapp.data.source.DefaultTasksRepository
import com.example.android.architecture.blueprints.todoapp.data.source.TasksDataSource
import com.example.android.architecture.blueprints.todoapp.data.source.TasksRepository
import com.example.android.architecture.blueprints.todoapp.data.source.local.TasksLocalDataSource
import com.example.android.architecture.blueprints.todoapp.data.source.local.ToDoDatabase
import com.example.android.architecture.blueprints.todoapp.data.source.remote.TasksRemoteDataSource
import kotlinx.coroutines.runBlocking

/**
 * Since the ServiceLocator is a singleton, it has the possibility of being accidentally shared
 * between tests. To help avoid this, create a method that properly resets the ServiceLocator
 * state between tests.
 *
 * Important: One of the downsides of using a service locator is that it is a shared singleton.
 * In addition to needing to reset the state of the service locator when the test finishes,
 * you cannot run tests in parallel.
 * */
object ServiceLocator {

    private val lock = Any()
    private var database: ToDoDatabase? = null

    @Volatile
    var tasksRepository: TasksRepository? = null
        @VisibleForTesting set// This annotation is a way to express that the reason the setter is public is because of testing.

    fun provideTasksRepository(context: Context): TasksRepository {
        synchronized(this) {
            return tasksRepository ?: createTasksRepository(context)
        }
    }

    private fun createTasksRepository(context: Context): TasksRepository {
        return DefaultTasksRepository(TasksRemoteDataSource, createLocalDataSource(context))
            .also { tasksRepository = it }
    }

    private fun createLocalDataSource(context: Context): TasksDataSource {
        database = database ?: createDatabase(context)
        return TasksLocalDataSource(database!!.taskDao())
    }

    private fun createDatabase(context: Context): ToDoDatabase {
        return Room.databaseBuilder(context, ToDoDatabase::class.java, "Tasks.db")
            .build().also { database = it }
    }

    @VisibleForTesting
    fun resetRepository() {
        synchronized(lock) {
            runBlocking { TasksRemoteDataSource.deleteAllTasks() }
            database?.apply {
                clearAllTables()
                close()
            }
            database = null
            tasksRepository = null
        }
    }
}