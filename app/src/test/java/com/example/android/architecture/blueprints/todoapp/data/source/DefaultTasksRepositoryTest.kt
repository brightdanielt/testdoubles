package com.example.android.architecture.blueprints.todoapp.data.source

import com.example.android.architecture.blueprints.todoapp.MainCoroutineRule
import com.example.android.architecture.blueprints.todoapp.data.Result
import com.example.android.architecture.blueprints.todoapp.data.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DefaultTasksRepositoryTest {
    private val task1 = Task("Title1", "Description1")
    private val task2 = Task("Title2", "Description2")
    private val task3 = Task("Title3", "Description3")
    private val remoteTasks = listOf(task1, task2).sortedBy { it.id }
    private val localTasks = listOf(task3).sortedBy { it.id }
    private val newTasks = listOf(task3).sortedBy { it.id }

    private lateinit var tasksLocaleDataSource: TasksDataSource
    private lateinit var tasksRemoteDataSource: TasksDataSource
    private lateinit var tasksRepository: DefaultTasksRepository

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    @Before
    fun createRepository() {
        tasksLocaleDataSource = FakeDataSource(localTasks.toMutableList())
        tasksRemoteDataSource = FakeDataSource(remoteTasks.toMutableList())
        tasksRepository =
            DefaultTasksRepository(tasksRemoteDataSource, tasksLocaleDataSource, Dispatchers.Main)
    }

    /**
     * Generally, only create one TestCoroutineDispatcher to run a test.
     * Whenever you call runBlockingTest, it will create a new TestCoroutineDispatcher
     * if you don't specify one. MainCoroutineRule includes a TestCoroutineDispatcher.
     * So, to ensure that you don't accidentally create multiple instances of TestCoroutineDispatcher,
     * use the mainCoroutineRule.runBlockingTest instead of just runBlockingTest.
     * */
    @Test
    fun getTasks_requestsAllTasksFromRemoteDataSource() = mainCoroutineRule.runBlockingTest {
        val tasks = tasksRepository.getTasks(true) as Result.Success
        assertThat(tasks.data, IsEqual(remoteTasks))
    }
}