package com.example.android.architecture.blueprints.todoapp.data.source

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.android.architecture.blueprints.todoapp.TodoApplication
import com.example.android.architecture.blueprints.todoapp.data.Task
import com.example.android.architecture.blueprints.todoapp.data.source.local.ToDoDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Which source set should you put your database tests in?
 * Note that in general, make database tests instrumented tests,meaning they will be in
 * the androidTest source set. This is because if you run these tests locally, they will
 * use whatever version of SQLite you have on your local machine, which could be very
 * different from the version of SQLite that ships with your Android device! Different Android
 * devices also ship with different SQLite versions, so it's helpful as well to be able to
 * run these tests as instrumented tests on different devices.*/
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
//This helps you group and choose which size test to run. DAO tests are considered
// unit tests since you are only testing the DAO, thus you can call them small tests.
@SmallTest
class TasksDaoTest {
    private lateinit var todoDatabase: ToDoDatabase

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initDb() {
        val appContext = ApplicationProvider.getApplicationContext<TodoApplication>()
        // Using an in-memory database so that the information stored here disappears when the
        // process is killed.
        todoDatabase = Room.inMemoryDatabaseBuilder(
            appContext,
            ToDoDatabase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        todoDatabase.close()
    }

    @Test
    fun insertTaskAndGetById() = runBlockingTest {
        // GIVEN - Insert a task.
        val task = Task("title", "description")
        todoDatabase.taskDao().insertTask(task)

        // WHEN - Get the task by id from the database.
        val loaded = todoDatabase.taskDao().getTaskById(task.id)

        // THEN - The loaded data contains the expected values.
        assertThat<Task>(loaded as Task, notNullValue())
        assertThat(loaded.id, `is`(task.id))
        assertThat(loaded.title, `is`(task.title))
        assertThat(loaded.description, `is`(task.description))
        assertThat(loaded.isCompleted, `is`(task.isCompleted))
    }

    @Test
    fun updateTaskAndGetById() = runBlockingTest {
        val task = Task("title", "description")
        task.description = "Hi"
        // 1. Insert a task into the DAO.
        todoDatabase.taskDao().insertTask(task)
        // 2. Update the task by creating a new task with the same ID but different attributes.
        todoDatabase.taskDao().updateTask(task)
        // 3. Check that when you get the task by its ID, it has the updated values.
        val loaded = todoDatabase.taskDao().getTaskById(task.id)
        assertThat(loaded as Task, notNullValue())
        assertThat(loaded.id, `is`(task.id))
        assertThat(loaded.title, `is`(task.title))
        assertThat(loaded.description, `is`(task.description))
        assertThat(loaded.isCompleted, `is`(task.isCompleted))
    }
}